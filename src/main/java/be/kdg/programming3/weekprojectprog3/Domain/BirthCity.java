package be.kdg.programming3.weekprojectprog3.Domain;

public class BirthCity {

    //Attributes
    private int cityID;
    private String cityName;
    private String countryName;
    private Long cityPopulation; //I needed a long value so we are also measuring the size of the birthplaces of nobel prize lauriates now

    //Constructor

    public BirthCity(int cityID, String cityName, String countryName, Long cityPopulation) {
        this.cityID = cityID;
        this.cityName = cityName;
        this.countryName = countryName;
        this.cityPopulation = cityPopulation;
    }

    //Getters


    public int getCityID() {
        return cityID;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public Long getCityPopulation() {
        return cityPopulation;
    }

    //Setters

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setCityPopulation(Long cityPopulation) {
        this.cityPopulation = cityPopulation;
    }

    //ToString


    @Override
    public String toString() {
        return "BirthCity{" +
                "cityID=" + cityID +
                ", cityName='" + cityName + '\'' +
                ", countryName='" + countryName + '\'' +
                ", cityPopulation=" + cityPopulation +
                '}';
    }
}
