package be.kdg.programming3.weekprojectprog3.Domain;

import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.util.Date;

public class Laureate {

    //Attributes
    private int laureateID,cityID;
    private String firstName;
    private String lastName;
    private LocalDateTime birthDate;


    //constructor


    public Laureate(int laureateID, String firstName, String lastName, LocalDateTime birthDate,int cityID) {
        this.laureateID = laureateID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.cityID=cityID;
    }

    //Getter

    public int getCityID() {
        return cityID;
    }

    public int getLaureateID() {
        return laureateID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    //Setter

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public void setLaureateID(int laureateID) {
        this.laureateID = laureateID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    //Tostring


    @Override
    public String toString() {
        return "Laureate{" +
                "laureateID=" + laureateID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
