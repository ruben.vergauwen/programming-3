package be.kdg.programming3.weekprojectprog3.Domain;

import java.util.List;

public class Prize {
    //Attributes
    private int prizeID;
    private List<Integer> winnerlist;

    private int year;
    private String name;
    private PrizeCategory category;


    //Constructor

    public Prize(int prizeID, List<Integer> winnerlist, int year, String name, PrizeCategory category) {
        this.prizeID = prizeID;
        this.winnerlist = winnerlist;
        this.year = year;
        this.name = name;
        this.category = category;
    }
    //Getter

    public int getPrizeID() {
        return prizeID;
    }

    public int getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public PrizeCategory getCategory() {
        return category;
    }

    public List<Integer> getWinnerlist() {
        return winnerlist;
    }
//Setter


    public void setPrizeID(int prizeID) {
        this.prizeID = prizeID;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(PrizeCategory category) {
        this.category = category;
    }

    public void setWinnerlist(List<Integer> winnerlist) {
        this.winnerlist = winnerlist;
    }

//Tostring


    @Override
    public String toString() {
        return "Prize{" +
                "prizeID=" + prizeID +
                ", winnerlist=" + winnerlist +
                ", year=" + year +
                ", name='" + name + '\'' +
                ", category=" + category +
                '}';
    }
}
