package be.kdg.programming3.weekprojectprog3;

import be.kdg.programming3.weekprojectprog3.Domain.BirthCity;
import be.kdg.programming3.weekprojectprog3.Domain.Laureate;
import be.kdg.programming3.weekprojectprog3.Domain.Prize;
import be.kdg.programming3.weekprojectprog3.Domain.PrizeCategory;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static be.kdg.programming3.weekprojectprog3.DataFactory.*;
import static be.kdg.programming3.weekprojectprog3.DataFactory.prizes;


public class Menu {
    private Scanner scanner = new Scanner(System.in);
    private JsonWriter jSonWriter = new JsonWriter();

    public static void showText() {
        System.out.println(" ");
        System.out.println("What would you like to do?");
        System.out.println("==========================");
        System.out.println("0) Quit");
        System.out.println("1) Show all laureate's");
        System.out.println("2) Show all laureates from city");
        System.out.println("3) Show all prizes");
        System.out.println("4) Show laureate's with name and/or date birth");
        System.out.println("Choice (0-4): ");


    }

    public void show() {

        DataFactory.fillLaureates();

        boolean startLoop = true;
        while (startLoop) {
            showText();
            int a = scanner.nextInt();

            switch (a) {
                case 0:
                    System.out.println("shutting down");
                    System.exit(0);
                    break;
                case 1:
                    // code block
                    System.out.println(laureates);
                    jSonWriter.writeLaureates(laureates);
                    System.out.println("Data is saved to Laureates.json...");


                    break;
                case 2:
                    //Show all laureates that were born in a specific city
                    System.out.println("Look for laureate's from the city:");
                    String b = scanner.next();
                    System.out.println(b);
                    List<BirthCity> results = cities.stream().filter(d -> d.getCityName().equals((b.substring(0, 1).toUpperCase() + b.substring(1).toLowerCase()))).collect(Collectors.toList());

                    for (BirthCity result : results) {
                        List<Laureate> LaureateFilteredByCity = laureates.stream().filter(c -> c.getCityID() == result.getCityID()).collect(Collectors.toList());
                        System.out.println(LaureateFilteredByCity);
                        jSonWriter.writeLaureates(LaureateFilteredByCity);
                    }

                    System.out.println("Data is saved to Laureates.json...");
//
                    break;
                case 3:
                    //Print all prizes
                    System.out.println(prizes);
                    jSonWriter.writePrizes(prizes);
                    System.out.println("Saved to prizes.json");
                    break;
                case 4:
                    // Show prizes from category and or year
                    System.out.println("Enter a category or leave blank");
                    System.out.println("PHYSICS = 0, MEDICINE = 1, PEACE = 2, LITERATURE = 3,CHEMISTRY = 4");
                    System.out.println("Choice (0-4)");

                    Scanner keyboard = new Scanner(System.in);
                    //To register the response with the option of leaving it blank
                    String c = null;

                    while (!(c = keyboard.nextLine()).isEmpty()) {
                        System.out.println(c);

                        break;

                    }

                    //To register the response with the option of leaving it blank
                    String d = null;
                    System.out.println("Enter a year or leave blank");
                    while (!(d = keyboard.nextLine()).isEmpty()) {
//
                        System.out.println(d);

                        break;

                    }
                    if (!c.isEmpty() && !d.isEmpty()) {
                        System.out.println("both are filled");

                        String finalC = c;
                        String finalD1 = d;

                        List<Prize> both =prizes.stream().filter(f -> f.getCategory() == PrizeCategory.values()[Integer.parseInt(finalC)]).filter(e -> e.getYear() == Integer.parseInt(finalD1)).collect(Collectors.toList());
                        System.out.println(both);
                        jSonWriter.writePrizes(both);
                        System.out.println("Saved to Prizes.json");

                    } else if (c.isEmpty()) {
                        System.out.println("year has a value");
                        String finalD1 = d;
                        List<Prize> result2 = prizes.stream().filter(e -> e.getYear() == Integer.parseInt(finalD1)).collect(Collectors.toList());
                        System.out.println(result2);
                        jSonWriter.writePrizes(result2);
                        System.out.println("Saved to prizes.json");
                    } else if (d.isEmpty()) {
                        System.out.println("Category is given");
                        String finalC = c;
                        List<Prize> result3=prizes.stream().filter(f -> f.getCategory() == PrizeCategory.values()[Integer.parseInt(finalC)]).collect(Collectors.toList());
                        System.out.println(result3);
                        jSonWriter.writePrizes(result3);
                        System.out.println("Saved to Prizes.json");
                    } else {
                        break;
                    }
                    break;
                default:

            }
        }

    }
}
