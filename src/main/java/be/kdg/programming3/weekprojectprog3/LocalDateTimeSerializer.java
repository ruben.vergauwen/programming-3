package be.kdg.programming3.weekprojectprog3;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//i could have made a serializer using the localDate format instead since i don't really need the exact time.
public class LocalDateTimeSerializer implements JsonSerializer<LocalDateTime> {
    private static final DateTimeFormatter FORMATTER
            = DateTimeFormatter.ofPattern("d-MMM-yyyy");
    @Override
    public JsonElement serialize(LocalDateTime localDateTime,
                                 Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(FORMATTER.format(localDateTime));
    }
}
