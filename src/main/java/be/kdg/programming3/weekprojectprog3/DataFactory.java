package be.kdg.programming3.weekprojectprog3;

import be.kdg.programming3.weekprojectprog3.Domain.BirthCity;
import be.kdg.programming3.weekprojectprog3.Domain.Laureate;
import be.kdg.programming3.weekprojectprog3.Domain.Prize;
import be.kdg.programming3.weekprojectprog3.Domain.PrizeCategory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataFactory {

    //Attributes
    public static List<Laureate> laureates;
    public static List<Prize> prizes;

    //This one wasn't in the assignment but i chose to make this the mandatory attribute i search on so i need to add data to it.
    public static List<BirthCity> cities;


    //methods

    //Fills the lists with data
    public static void seed() {

            //Fill laureate lists
            Laureate a = new Laureate(1, "Jacobus", "van 't Hoff", LocalDateTime.of(1852,8,23,0, 0),1);
            Laureate b = new Laureate(2, "Sully", "Prudhomme", LocalDateTime.of(1839, 3, 16,0,0),2);
            Laureate c = new Laureate(3, "Marie", "Curie", LocalDateTime.of(1867, 11, 7,0,0),3);
            Laureate d = new Laureate(4, "Pierre", "Curie", LocalDateTime.of(1859, 5, 15,0,0),2);
            laureates.add(a);
            laureates.add(b);
            laureates.add(c);
            laureates.add(d);

            //Fill prizes list
            Prize e = new Prize(1, Arrays.asList(1),1901,"The nobel prize in chemistry 1901", PrizeCategory.CHEMISTRY);
            Prize f = new Prize(2, Arrays.asList(2),1901,"The Nobel Prize in Literature 1901",PrizeCategory.LITERATURE);
            Prize g = new Prize(3, Arrays.asList(3,4),1903,"The nobel prize in chemistry 1903",PrizeCategory.CHEMISTRY);
            Prize h = new Prize(4, Arrays.asList(3,4),1903,"The nobel prize in physics 1903",PrizeCategory.PHYSICS);

            prizes.add(e);
            prizes.add(f);
            prizes.add(g);
            prizes.add(h);



            //Fill cities list
        BirthCity i = new BirthCity(1,"Leiden","Netherlands", (long) 800000);
        BirthCity j = new BirthCity(2,"Paris","France", (long) 2000000);
        BirthCity k = new BirthCity(3,"Warschaw","poland", (long) 600000);
        cities.add(i);
        cities.add(j);
        cities.add(k);


        

    }
    @Autowired
    public static void fillLaureates() {
        laureates = new ArrayList<>();
        prizes = new ArrayList<>();
        cities = new ArrayList<>();
        seed();
    }
}
