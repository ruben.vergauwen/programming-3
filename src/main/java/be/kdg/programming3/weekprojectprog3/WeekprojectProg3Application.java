package be.kdg.programming3.weekprojectprog3;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeekprojectProg3Application {
    public static void main(String[] args) {
        SpringApplication.run(WeekprojectProg3Application.class, args);

        //Console Application
        Menu menu = new Menu();
        menu.show();


    }
}
