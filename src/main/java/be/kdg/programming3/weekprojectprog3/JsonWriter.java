package be.kdg.programming3.weekprojectprog3;

import be.kdg.programming3.weekprojectprog3.Domain.Laureate;
import be.kdg.programming3.weekprojectprog3.Domain.Prize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import static be.kdg.programming3.weekprojectprog3.DataFactory.laureates;


public class JsonWriter {
    private static final String LAUREATES_JSON = "laureates.json";
    private static final String PRIZES_JSON = "prizes.json";
    private Gson gson;

    public JsonWriter(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        //setPrettyPrinting will add indentation to the JSON file
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
        gson = gsonBuilder.create();
    }

// Write laureates to json
public void writeLaureates(List<Laureate> laureates){
    String json = gson.toJson(laureates);
    try (FileWriter writer = new FileWriter(LAUREATES_JSON)) {
        writer.write(json);
    } catch (IOException e) {
        throw new RuntimeException("Unable to save Laureates to JSON", e);
    }
}
//Write prizes to json
public void writePrizes(List<Prize> prizes){
    String json = gson.toJson(prizes);
    try (FileWriter writer = new FileWriter(PRIZES_JSON)) {
        writer.write(json);
    } catch (IOException e) {
        throw new RuntimeException("Unable to save observations to JSON", e);
    }
}

//    Gson gson = gsonBuilder.create();
//    String jsonString = gson.toJson(laureates);
//                    System.out.println(jsonString);
}
